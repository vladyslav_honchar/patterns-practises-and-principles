import checkUniqueness from './socketHelper/usernameValidation.mjs';
import getRooms from './socketHelper/getRooms.mjs';
import createRoomButton from './socketHelper/createRoomButton.mjs';
import parseOnlineCounter from './socketHelper/parseOnline.mjs';
import updateRooms from './socketHelper/updateRooms.mjs';
import roomUsersLogic from './socketHelper/roomUsers.mjs';
import gameProcess from './socketHelper/gameProcess.mjs';
import gameProgress from './socketHelper/gameProgress.mjs';
import stopGame from './socketHelper/stopGame.mjs';

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

checkUniqueness(socket, username);
createRoomButton(socket);
getRooms(socket);
parseOnlineCounter(socket);
updateRooms(socket);
roomUsersLogic(socket);
gameProgress(socket);
gameProcess(socket);
stopGame(socket);








