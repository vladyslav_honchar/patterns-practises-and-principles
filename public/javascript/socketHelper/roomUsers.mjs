

import addUserInRoom, { showGamePage, DeleteUserFromRoom } from '../helpers/roomUsersHelper.mjs';


export default (socket) => {
    socket.on('GET_ALL_USERS_IN_ROOM', (usernames, roomName) => {
        usernames.forEach((username) => {
            addUserInRoom(username);
            setReadyStatus(socket, username);
        });
        showGamePage(socket, sessionStorage.username, roomName);
    });
    socket.on('ADD_USER_IN_ROOM', (username) => {
        addUserInRoom(username);
        setReadyStatus(socket, username);
    });
    socket.on('DELETE_USER_FROM_ROOM', (username) => {
        DeleteUserFromRoom(username);
    });
    socket.on('CHECK_READY', (username, status) => {
        const readyStatus = document.getElementById(`${username}-ready`);

        if (status) {
            if (readyStatus.className.indexOf("ready-status-red") != -1) {
                readyStatus.className = readyStatus.className.replace(
                    'ready-status-red',
                    'ready-status-green'
                );
            } else {
                readyStatus.className = readyStatus.className.concat(" ready-status-green");
            }
        } else {
            if (readyStatus.className.indexOf("ready-status-green") != -1) {
                readyStatus.className = readyStatus.className.replace(
                    'ready-status-green',
                    'ready-status-red'
                );
            } else {
                readyStatus.className = readyStatus.className.concat(" unready");
            }
        }
    });
};

function setReadyStatus(socket, username) {
    socket.emit('IS_READY', username);
}
