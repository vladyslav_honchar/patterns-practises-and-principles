import { createElement } from "../helpers/createElement.mjs";
import startTyping from "../helpers/changeTextHelper.mjs";

export default (socket) => {
  let text;
  let timerBeforeGame;
  socket.on('GET_READY_STATUS', (username) => {
    changeReadyColor(username);
  });

  socket.on('START_GAME', (textId) => {
    hideButtons();
    showTimerBeforeStart();
    timerBeforeGame = document.getElementById('timer-before');
  });
  socket.on('ALTER_TIMER_BEFORE_START', (second) => {
    changeTimerBeforeStartValue(timerBeforeGame, second);
    if (second == 0) {
      hideTimerBeforeStart();
      showGame(text);
      startTyping(socket);
    }
  });
  socket.on('GET_TEXT', async (textId) => {
    text = await getText(textId);
  });
  socket.on('ALTER_GAME_TIMER', (second) => {
    changeGameTimer(second);
  });
};

const hideButtons = () => {
  const readyButton = document.getElementById("ready-btn");
  const backButton = document.getElementById("quit-room-btn");
  readyButton.style.display = "none";
  backButton.style.display = "none";
}
const showGame = (text) => {
  const gameBlock = document.getElementById("game-block");
  const gameTimer = createElement({
    tagName: "span",
    className: "game-timer",
    attributes: {
      id: "game-timer",
    },
  });
  const textBlock = createElement({
    tagName: "div",
    className: "",
    attributes: {
      id: "text-container",
    },
  });
  const coloredText = createElement({
    tagName: "span",
    className: "",
    attributes: {
      id: "colored-text",
    },
  });
  const selectedText = createElement({
    tagName: "span",
    className: "",
    attributes: {
      id: "selected-text",
    },
  });
  const undecoratedText = createElement({
    tagName: "span",
    className: "",
    attributes: {
      id: "undecorated-text",
    },
  });

  undecoratedText.innerText = text;

  textBlock.append(coloredText);
  textBlock.append(selectedText);
  textBlock.append(undecoratedText);

  gameBlock.append(gameTimer);
  gameBlock.append(textBlock);
}
const showTimerBeforeStart = () => {
  const gameBlock = document.getElementById("game-block");
  const timer = createElement({
    tagName: "div",
    className: "timer-before",
    attributes: {
      id: "timer-before",
    },
  });
  gameBlock.append(timer);
}

const changeTimerBeforeStartValue = (timer, value) => {
  timer.innerText = value;
}

const changeGameTimer = (value) => {
  const timer = document.getElementById('game-timer');
  timer.innerText = `${value} seconds left`;
}

const hideTimerBeforeStart = () => {
  const timer = document.getElementById('timer-before');
  timer.remove();
}

const changeReadyColor = (username) => {
  const readyStatus = document.getElementById(`${username}-ready`);
  const classNamesArr = readyStatus.className.split(" ");
  const readyIndex = classNamesArr.indexOf("ready-status-green");
  if (readyIndex == -1) {
    const unreadyIndex = classNamesArr.indexOf('ready-status-red');
    classNamesArr.splice(unreadyIndex, 1);
    classNamesArr.push('ready-status-green');
  } else {
    classNamesArr.splice(readyIndex, 1);
    classNamesArr.push('ready-status-red');
  }
  const classNamesStr = classNamesArr.join(' ');
  readyStatus.className = classNamesStr;
}

const getText = async (id) => {
  const res = await fetch(`http://localhost:3002/game/texts/${id}`, { mode: 'no-cors'});
  const response = await res.json();
  const data = response.data;
  return data;
}
