import addRoomToPage from "../helpers/addRoomToPage.mjs";
import addClickListenerJoinRoom, { joinRoom } from "../helpers/joinButton.mjs";

export default (socket) => {
    const createRoomButton = document.getElementById('add-room-btn');
    const addRoomButton = () => {
        const roomId = prompt(
            'Write an id for the room you want to create'
        );
        if (roomId) {
            socket.emit('ADD_ROOM_REQ', roomId);
        } else {
            alert('Invalid id');
            return;
        }
    };
    createRoomButton.addEventListener("click", addRoomButton);

    socket.on('ADD_ROOM_RES', (roomId) => {
        joinRoom(socket, roomId, sessionStorage.username);
    });
    socket.on('UPDATE_ADDED_ROOM', (roomId) => {
        addRoomToPage(roomId);
        addClickListenerJoinRoom(socket, roomId);
    });
    socket.on('ADD_ROOM_BAD_RES', () => {
        alert(
            'Room with this name already exists'
        );
    });
}