import { createElement }  from "./createElement.mjs";

const roomsUnit = document.getElementById('rooms-unit');

export default (roomName) => {
    const room = createElement({
        tagName: "div",
        className: "room",
        attributes: {
            id: `${roomName}-room-card`
        }
    });

    const roomTitle = createElement({
        tagName: "span",
        className: "room-title",
    });
    roomTitle.innerText = roomName;
    const joinRoomButton = createElement({
        tagName: "button",
        className: "btn join-btn",
        attributes: {
            id: `${roomName}-join`,
        },
    });
    joinRoomButton.innerText = "Join";
    const roomOnline = createElement({
        tagName: "span",
        className: "room-online",
        attributes: {
            id: `${roomName}-online`,
        },
    });

    roomOnline.innerText = "1 in room"

    room.append(roomTitle);
    room.append(roomOnline);
    room.append(joinRoomButton);

    roomsUnit.append(room);
}