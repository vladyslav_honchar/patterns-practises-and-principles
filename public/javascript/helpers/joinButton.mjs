export function joinRoom(socket, roomId, username) {
    socket.emit("JOIN_ROOM", roomId);
}

export default function addClickListenerJoinRoom(socket, roomId) {
    const joinButton = document.getElementById(`${roomId}-join`);

    joinButton.addEventListener("click", () => {
        joinRoom(socket, roomId);
    });
}
