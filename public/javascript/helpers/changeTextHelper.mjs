export let keyDownFunc;

export default function startTyping(socket) {
  const colored = document.getElementById("colored-text");
  const selected = document.getElementById("selected-text");
  const undecorated = document.getElementById("undecorated-text");

  selected.innerText = undecorated.innerText[0];

  undecorated.innerText = undecorated.innerText.slice(1);

  function keyDown(event) {
    let pressedButton = event.key;
    if (event.key === ' ') pressedButton = "\xa0";

    if (pressedButton === selected.innerText) {
      colored.innerText = colored.innerText.concat(selected.innerText);
      socket.emit('KEY_PRESSED', colored.innerText.length);

      if (undecorated.innerText.length == 0) {
        selected.innerText = '';
        return;
      }

      if (undecorated.innerText[0] != ' ') {
        selected.innerText = undecorated.innerText[0];
      } else {
        selected.innerText = "\xa0";
      }
      undecorated.innerText = undecorated.innerText.slice(1);
    }
  }
  keyDownFunc = keyDown
  document.addEventListener("keydown", keyDown);
}
