import { Server, Socket } from "socket.io";

export default (io: Server, socket: Socket) => {
  socket.on("ONLINE_IN_ROOM", (roomId: string): void => {
    const room = io.sockets.adapter.rooms[roomId];
    console.log(room);
    
    if (room) {
      const roomClients = room.length;
      socket.emit('GET_ROOM_ONLINE', roomId, roomClients);
    } else {
      socket.emit('GET_ROOM_ONLINE', roomId, 0);
    }
  });
};
