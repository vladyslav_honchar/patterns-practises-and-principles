import { Socket, Server } from 'socket.io';
import { roomsMap } from '../../helpers/roomHelper'
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";

export default (io: Server, socket: Socket) => {
    socket.on('JOIN_ROOM', (roomId: string): void => {
      joinUser(io, socket, roomId);
    });
  };

  const joinUser = (io: Server, socket: Socket, roomId: string): void => {
    const username: string = socket.handshake.query.username;
    const usersInRoom = roomsMap.get(roomId);
    usersInRoom?.add({
      id: socket.id,
      ready: false,
      username,
      progress: 0,
    });
    socket.join(roomId);
    roomsMap.set(roomId, usersInRoom!);
    io.emit('GET_ROOM_ONLINE', roomId, usersInRoom?.size);
    if (MAXIMUM_USERS_FOR_ONE_ROOM === usersInRoom?.size) {
      io.emit('DISAPPEAR_ROOM', roomId);
    }
    socket.emit('GET_ALL_USERS_IN_ROOM', getAllUsernamesInRoom(roomId), roomId);
    socket.to(roomId).emit('ADD_USER_IN_ROOM', username);
  };
  
  function getAllUsernamesInRoom(roomId: string) {
    const usersInRoom = roomsMap.get(roomId);
    let usernames: string[] = [];
    usersInRoom?.forEach((user) => {
      usernames.push(user.username);
    });
    return usernames;
  }