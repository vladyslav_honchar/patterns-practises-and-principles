import { Server, Socket } from "socket.io";
import { texts } from "../../data";
import { getCurrentRoomId, findUserInRoom, IUser, roomsMap } from "../../helpers/roomHelper";
import { stopGame } from "./stopGame";
import { addUserToResults } from "./results";

let textLength: number;

export default (io: Server, socket: Socket) => {
  socket.on('KEY_PRESSED', (numberOfLetter) => {
    const progress = getProgress(textLength, numberOfLetter);
    changeUserProgress(socket, progress);
    const roomId = getCurrentRoomId(socket);
    const username = socket.handshake.query.username;
    if (progress == 100) addUserToResults(roomId!, username)
    
    io.to(roomId!).emit('CHANGE_USER_PROGRESS', username, progress);
    if (canWeStop(socket)) {
      stopGame(io, socket);
    }
  });
};

export function setTextLength(textId: number): void {
  textLength = texts[textId].length;
  return;
}

function getProgress(length: number, number: number) {
  return Math.ceil((100 * number) / length);
}

function changeUserProgress(socket: Socket, progress: number) {
  const user: IUser | undefined = findUserInRoom(socket);
  if (user) {
    user.progress = progress;
  }
}

export function canWeStop(socket: Socket) {
  let flag = true;
  const roomId = getCurrentRoomId(socket);
  const room = roomsMap.get(roomId!);
  room!.forEach((user) => {
    if (user.progress < 100) {
      flag = false;
      return;
    }
  });
  return flag;
}

export function canWeStopByRoomId(roomId: string) {
  let flag = true;
  const room = roomsMap.get(roomId!);
  room!.forEach((user) => {
    if (user.progress < 100) {
      flag = false;
      return;
    }
  });
  return flag;
}
