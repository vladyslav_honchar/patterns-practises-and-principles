import { Socket, Server } from "socket.io";
import {
  roomsMap,
  getCurrentRoomId,
  findUserInRoom,
  getRoomStatus,
  clearAllRoomTimers,
} from "../../helpers/roomHelper";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";
import { canWeStopByRoomId } from "./gameProgress";
import { stopGame } from "./stopGame";
import { deleteUserFromResults } from "./results";

export default (io: Server, socket: Socket) => {
  socket.on("LEAVE_FROM_ROOM", (): void => {
    leaveUser(io, socket);
  });
};

export const leaveUser = (io: Server, socket: Socket): void => {
  const roomId: string | undefined = getCurrentRoomId(socket);
  if (roomId) {
    const username = socket.handshake.query.username;
    const usersInRoom = roomsMap.get(roomId);
    usersInRoom!.delete(findUserInRoom(socket)!);
    deleteUserFromResults(roomId, username)
    socket.to(roomId).emit('DELETE_USER_FROM_ROOM', username);
    socket.leave(roomId);
    roomsMap.set(roomId, usersInRoom!);
    if (usersInRoom!.size === 0) {
      deleteRoom(io, roomId);
      clearAllRoomTimers(roomId);
      return;
    }
    if (
      usersInRoom!.size <= MAXIMUM_USERS_FOR_ONE_ROOM - 1 &&
      getRoomStatus(roomId) === 'created'
    ) {
      io.emit('APPEAR_ROOM', roomId);
    }
    io.emit('GET_ROOM_ONLINE', roomId, usersInRoom!.size);
    if (getRoomStatus(roomId) === 'started' && canWeStopByRoomId(roomId)) {
      stopGame(io, socket, roomId);
    }
  }
};

const deleteRoom = (io: Server, roomId: string): void => {
  roomsMap.delete(roomId);
  io.emit('DELETE_ROOM', roomId);
};
