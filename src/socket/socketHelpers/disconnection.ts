import { Socket, Server } from 'socket.io';
import { leaveUser } from './leaveRoom';
import { deleteUser } from './usernameUniqueness';

export default (io:Server, socket: Socket, username: string) => {
    socket.on('disconnect', () => {
        leaveUser(io, socket);
        deleteUser(username);
    })
    
}