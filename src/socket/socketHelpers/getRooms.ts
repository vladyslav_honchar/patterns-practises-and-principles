import { Server } from "socket.io";
import { Socket } from "socket.io";
import { roomsMap, getRoomStatus } from "../../helpers/roomHelper";
import { MAXIMUM_USERS_FOR_ONE_ROOM as maxUsers } from "../config";

export default (io: Server, socket: Socket) => {
    socket.on('GET_ROOMS_REQ', (): void => {
        const rooms: string[] = [];
        roomsMap.forEach((userSet, roomId) => {
            const roomStatus = getRoomStatus(roomId)
            if (userSet.size < maxUsers && roomStatus != "started" && userSet.size > 0) {
                rooms.push(roomId);
            }
        });
        socket.emit('GET_ROOMS_RES', rooms);
    });
}